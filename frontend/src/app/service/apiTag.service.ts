import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Tag} from '../model/tag';

@Injectable()
export class ApiTagService {

  constructor(
    private http: HttpClient
  ) {
  }

  getAllTags(): Observable<Tag[]> {
    return this.http.get<Tag[]>('http://localhost:8080/tags');
  }

}

