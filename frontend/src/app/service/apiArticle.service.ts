import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DisplayableSingleArticle} from '../model/displayable-single-article';
import {Article} from '../model/article';
import {NewBodyArticle} from '../model/new-body-article';
import {createUrlResolverWithoutPackagePrefix} from '@angular/compiler';
import {ConsultedBody} from '../model/consulted-body';

@Injectable()
export class ApiArticleService {

  constructor(
    private http: HttpClient
  ) {
  }

  getAllPublishedArticles(): Observable<Article[]> {
    return this.http.get<Article[]>('http://localhost:8080/articles');
  }

  getAllDraftsArticles(): Observable<Article[]> {
    return this.http.get<Article[]>('http://localhost:8080/articles/drafts');
  }

  getAllListsArticles(): Observable<Article[]> {
    return this.http.get<Article[]>('http://localhost:8080/articles/all');
  }

  getOneArticle(id: string): Observable<DisplayableSingleArticle> {
    return this.http.get<DisplayableSingleArticle>(`http://localhost:8080/articles/${id}`);
  }

  getAllDraftsByAuthor(id: string): Observable<Article[]> {
    return this.http.get<Article[]>(`http://localhost:8080/articles/drafts/${id}`);
  }

  getAllArticlesPublishedByAuthor(id: string): Observable<Article[]> {
    return this.http.get<Article[]>(`http://localhost:8080/articles/published/${id}`);
  }

  getAllArticlesAllByAuthor(id: string): Observable<Article[]> {
    return this.http.get<Article[]>(`http://localhost:8080/articles/all/${id}`);
  }

  getPubArticlesByCategory(id: string): Observable<Article[]> {
    return this.http.get<Article[]>(`http://localhost:8080/articles/published/category/${id}`);
  }

  getDraftsByCategory(id: string): Observable<Article[]> {
    return this.http.get<Article[]>(`http://localhost:8080/articles/drafts/category/${id}`);
  }

  getAllArticlesByCategory(id: string): Observable<Article[]> {
    return this.http.get<Article[]>(`http://localhost:8080/articles/all/category/${id}`);
  }

  getPubArticlesByTag(id: string): Observable<Article[]> {
    return this.http.get<Article[]>(`http://localhost:8080/articles/published/tag/${id}`);
  }

  getDraftsByTag(id: string): Observable<Article[]> {
    return this.http.get<Article[]>(`http://localhost:8080/articles/drafts/tag/${id}`);
  }

  getAllByTag(id: string): Observable<Article[]> {
    return this.http.get<Article[]>(`http://localhost:8080/articles/all/tag/${id}`);
  }

  getCompleteArticle(id: string): Observable<Article> {
    return this.http.get<Article>(`http://localhost:8080/articles/complete/${id}`);
  }

  getConsultedArticles(): Observable<Article[]> {
    return this.http.get<Article[]>('http://localhost:8080/articles/all/consulted');
  }

  createArticle(article: NewBodyArticle): Observable<NewBodyArticle> {
    return this.http.post<NewBodyArticle>('http://localhost:8080/articles', article);
  }

  publishArticle(id: string, published: string): Observable<Article> {
    return this.http.put<Article>(`http://localhost:8080/articles/status/${id}`, published);
  }

  isArticleConsulted(id: string, isConsulted: ConsultedBody): Observable<Article> {
    return this.http.put<Article>(`http://localhost:8080/articles/consulted/${id}`, isConsulted);
  }

  modifyArticle(id: string, body: NewBodyArticle): Observable<Article> {
    return this.http.put<Article>(`http://localhost:8080/articles/${id}`, body);
  }

  getArticleByTitle(title: string): Observable<Article> {
    return this.http.post<Article>(`http://localhost:8080/articles/title`, title);
  }

  deleteArticle(id: string): Observable<Article> {
    return this.http.delete<Article>(`http://localhost:8080/articles/${id}`);
  }


}
