import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NewBodyAuthor} from '../model/new-body-author';
import {Observable} from 'rxjs';
import {Author} from '../model/author';

@Injectable()
export class ApiAuthorService {

  constructor(
    private http: HttpClient
  ) {
  }

  createAuthor(user: NewBodyAuthor): Observable<NewBodyAuthor> {
    return this.http.post<NewBodyAuthor>('http://localhost:8080/users', user);
  }

  doesUserExists(data: NewBodyAuthor): Observable<boolean> {
    // @ts-ignore
    return this.http.request<boolean>('http://localhost:8080/users', data);
  }

  getByMail(mail: string): Observable<Author> {
    return this.http.get<Author>(`http://localhost:8080/users/${mail}`);
  }

  getAllAuthors(): Observable<Author[]> {
    return this.http.get<Author[]>('http://localhost:8080/users/all');
  }

}
