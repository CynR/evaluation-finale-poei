import {Component, Input, OnInit, OnChanges} from '@angular/core';
import {ApiArticleService} from '../../service/apiArticle.service';
import {Category} from '../../model/category';
import {ApiCategoryService} from '../../service/apiCategory.service';
import {NewBodyTag} from '../../model/new-body-tag';
import {NewBodyArticle} from '../../model/new-body-article';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.css']
})

export class CreateArticleComponent implements OnInit, OnChanges {

  @Input() idCurrentUser;
  listCategories: Category[];
  title = '';
  overview = '';
  content = '';
  categoryId = '';
  tagString = '';
  listTags: NewBodyTag[] = [];
  // messages
  showMessageIncomplete = false;
  MessageIncomplete = 'Please insert all required information';
  showMessageSuccess = false;
  MessageSuccess = 'Article Created';
  showErrorOverview = false;
  MessageErrorOverview = `The 'Overview' can have maximum 50 words`;

  constructor(
    private apiArticleService: ApiArticleService,
    private apiCategoryService: ApiCategoryService
  ) {
  }

  ngOnInit() {
    this.apiCategoryService.getAllCategories().subscribe(
      answer => {
        this.listCategories = answer;
      });
  }

  ngOnChanges() {
    let numberOfWordsInOverview = 0;
    for (const overviewWords of this.overview.split(' ')) {
      numberOfWordsInOverview = numberOfWordsInOverview + 1;
    }
    if (numberOfWordsInOverview > 50) {
      this.showErrorOverview = true;
    } else {
      this.showErrorOverview = false;
    }
  }


  createDraft() {
    if (this.title.length > 0 || this.overview.length > 0 || this.content.length > 0) {
      for (const tagToSeparate of this.tagString.split(',')) {
        this.listTags.push(new NewBodyTag(tagToSeparate.trim()));
      }
      this.apiArticleService.createArticle(new NewBodyArticle(this.title, this.overview,
        this.content, this.categoryId, this.idCurrentUser, this.listTags)).subscribe();
      this.showMessageSuccess = true;
      this.showMessageIncomplete = false;
      this.whiteTextBoxes();
    } else {
      this.showMessageIncomplete = true;
      this.showMessageSuccess = false;
    }
  }

  maxOverview(overview: string) {
    let numberOfWordsInOverview = 0;
    for (const overviewWords of overview.split(' ')) {
      numberOfWordsInOverview = numberOfWordsInOverview + 1;
    }
    if (numberOfWordsInOverview > 50) {
      this.showErrorOverview = true;
    } else {
      this.showErrorOverview = false;
    }
  }

  whiteTextBoxes() {
    this.title = '';
    this.overview = '';
    this.content = '';
    this.categoryId = '';
    this.tagString = '';
  }

}
