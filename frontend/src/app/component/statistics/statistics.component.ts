import {Component, OnInit} from '@angular/core';
import {ChartDataSets, ChartOptions} from 'chart.js';
import {Color, Label} from 'ng2-charts';
import {ApiArticleService} from '../../service/apiArticle.service';
import {ApiAuthorService} from '../../service/apiAuthor.service';
import {ApiCategoryService} from '../../service/apiCategory.service';
import {ApiTagService} from '../../service/apiTag.service';
import {Author} from '../../model/author';
import {Category} from '../../model/category';
import {Tag} from '../../model/tag';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
  // graphic Articles
  numberOfDraftsArticles: number;
  numberOfPublishedArticles: number;
  numberOfArticles: number;
  dataArticles: number[] = [];
  // graphic Authors
  numberOfDraftsArticlesByAuthor: number;
  numberOfPublishedArticlesByAuthor: number;
  numberOfArticlesByAuthor: number;
  listAuthors: Author[];
  id: string;
  dataArticlesAuthors: number[] = [];
  lineChartLabels: Label[] = ['All Articles', 'Drafts Total', 'Published Total'];
  // all charts
  lineChartType = 'doughnut';
  lineChartOptions = {
    responsive: true,
  };
  lineChartLegend = true;
  // charts for articles and authors
  lineChartColors: Color[] = [
    {
      backgroundColor: [
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)'
      ],
      borderColor: [
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)'
      ],
      borderWidth: 2,
    },
  ];
  // graphic Categories
  numberOfDraftsArticlesByCat: number;
  numberOfPublishedArticlesByCat: number;
  numberOfArticlesByCat: number;
  listCategories: Category[];
  categoryId = '';
  dataArticlesCategories: number[] = [];
  lineChartCategories: Label[] = ['All Articles', 'Drafts Total', 'Published Total'];
  lineChartColorsCat: Color[] = [
    {
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(255, 159, 64, 0.2)',
        'rgba(75, 192, 192, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(255, 159, 64, 1)',
        'rgba(75, 192, 192, 1)'
      ],
      borderWidth: 2,
    },
  ];

  // graphic Tags
  numberOfDraftsArticlesByTag: number;
  numberOfPublishedArticlesByTag: number;
  numberOfArticlesByTag: number;
  listTags: Tag[];
  tagId = '';
  dataArticlesTags: number[] = [];
  lineChartTags: Label[] = ['All Articles', 'Drafts Total', 'Published Total'];


  constructor(
    private apiArticleService: ApiArticleService,
    private apiAuthorService: ApiAuthorService,
    private apiCategoryService: ApiCategoryService,
    private apiTagService: ApiTagService
  ) {
  }

  ngOnInit() {
    this.apiArticleService.getAllListsArticles().subscribe(answer => {
      this.numberOfArticles = answer.length;
      this.dataArticles.push(this.numberOfArticles);
      this.apiArticleService.getAllDraftsArticles().subscribe(answer2 => {
        this.numberOfDraftsArticles = answer2.length;
        this.dataArticles.push(this.numberOfDraftsArticles);
        this.apiArticleService.getAllPublishedArticles().subscribe(answer3 => {
          this.numberOfPublishedArticles = answer3.length;
          this.dataArticles.push(this.numberOfPublishedArticles);
          // console.log(this.dataArticles);
        });
      });
    });
    this.apiAuthorService.getAllAuthors().subscribe(answer => {
      this.listAuthors = answer;
    });
    this.apiCategoryService.getAllCategories().subscribe(answer => {
      this.listCategories = answer;
    });
    this.apiTagService.getAllTags().subscribe((answer => {
      this.listTags = answer;
    }));
  }

  authorSelected(id: string) {
    this.dataArticlesAuthors = [];
    this.apiArticleService.getAllArticlesAllByAuthor(id).subscribe(answer1 => {
      this.numberOfArticlesByAuthor = answer1.length;
      this.dataArticlesAuthors.push(this.numberOfArticlesByAuthor);
      this.apiArticleService.getAllDraftsByAuthor(id).subscribe(answer2 => {
        this.numberOfDraftsArticlesByAuthor = answer2.length;
        this.dataArticlesAuthors.push(this.numberOfDraftsArticlesByAuthor);
        this.apiArticleService.getAllArticlesPublishedByAuthor(id).subscribe(answer3 => {
          this.numberOfPublishedArticlesByAuthor = answer3.length;
          this.dataArticlesAuthors.push(this.numberOfPublishedArticlesByAuthor);
          // console.log(this.dataArticlesAuthors);
        });
      });
    });
  }

  categorySelected(id: string) {
    this.dataArticlesCategories = [];
    this.apiArticleService.getAllArticlesByCategory(id).subscribe(answer1 => {
      this.numberOfArticlesByCat = answer1.length;
      this.dataArticlesCategories.push(this.numberOfArticlesByCat);
      this.apiArticleService.getDraftsByCategory(id).subscribe(answer2 => {
        this.numberOfDraftsArticlesByCat = answer2.length;
        this.dataArticlesCategories.push(this.numberOfDraftsArticlesByCat);
        this.apiArticleService.getPubArticlesByCategory(id).subscribe(answer3 => {
          this.numberOfPublishedArticlesByCat = answer3.length;
          this.dataArticlesCategories.push(this.numberOfPublishedArticlesByCat);
          // console.log(this.dataArticlesCategories);
        });
      });
    });
  }

  tagSelected(id: string) {
    this.dataArticlesTags = [];
    this.apiArticleService.getAllByTag(id).subscribe( answer1 => {
      this.numberOfArticlesByTag = answer1.length;
      this.dataArticlesTags.push(this.numberOfArticlesByTag);
      this.apiArticleService.getDraftsByTag(id).subscribe( answer2 => {
        this.numberOfDraftsArticlesByTag = answer2.length;
        this.dataArticlesTags.push(this.numberOfDraftsArticlesByTag);
        this.apiArticleService.getPubArticlesByTag(id).subscribe( answer3 => {
          this.numberOfPublishedArticlesByTag = answer3.length;
          this.dataArticlesTags.push(this.numberOfPublishedArticlesByTag);
          console.log(this.dataArticlesTags);
        });
      });
    });
  }

}
