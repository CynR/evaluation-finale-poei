import {Component, Input, OnInit} from '@angular/core';
import {ApiArticleService} from '../../service/apiArticle.service';
import {Article} from '../../model/article';

@Component({
  selector: 'app-delete-article',
  templateUrl: './delete-article.component.html',
  styleUrls: ['./delete-article.component.css']
})
export class DeleteArticleComponent implements OnInit {

  @Input() idCurrentUser;
  listThisUserAllArticles: Article[];
  idArticle = '';
  showMessageSuccess = false;
  MessageSuccess = 'Article Deleted';

  constructor(
    private apiArticleService: ApiArticleService
  ) {
  }

  ngOnInit() {
    this.apiArticleService.getAllArticlesAllByAuthor(
      this.idCurrentUser).subscribe( answer => {
        this.listThisUserAllArticles = answer;
    });
  }

  deleteArticle() {
    this.apiArticleService.deleteArticle(this.idArticle)
      .subscribe();
    this.showMessageSuccess = true;
    setTimeout( () => {
      this.showMessageSuccess = false;
      this.idArticle = '';
    }, 2000);
  }

}
