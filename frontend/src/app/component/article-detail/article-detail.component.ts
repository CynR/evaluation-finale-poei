import {Component, Input, OnInit} from '@angular/core';
import {Article} from '../../model/article';
import {DisplayableSingleArticle} from '../../model/displayable-single-article';
import {ApiArticleService} from '../../service/apiArticle.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit {

  @Input() articleWithContent: DisplayableSingleArticle;

  constructor(
    private apiArticleService: ApiArticleService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    const idToSend = this.route.snapshot.paramMap.get('id');
    this.apiArticleService.getOneArticle(idToSend).subscribe(
      answer => {
        this.articleWithContent = answer;
      });
  }

}
