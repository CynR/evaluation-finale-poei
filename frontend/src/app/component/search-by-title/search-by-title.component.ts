import {Component, Input, OnInit} from '@angular/core';
import {Article} from '../../model/article';
import {ApiArticleService} from '../../service/apiArticle.service';

@Component({
  selector: 'app-search-by-title',
  templateUrl: './search-by-title.component.html',
  styleUrls: ['./search-by-title.component.css']
})
export class SearchByTitleComponent implements OnInit {

  titleSearched = '';
  artSearched: Article;

  constructor(
    private apiArticleService: ApiArticleService
  ) {
  }

  ngOnInit() {
  }

  search(title: string) {
    this.apiArticleService.getArticleByTitle(title).subscribe(answer => {
      this.artSearched = answer;
    });
  }

}
