import {Component, Input, OnInit} from '@angular/core';
import {ApiArticleService} from '../../service/apiArticle.service';
import {ApiCategoryService} from '../../service/apiCategory.service';
import {ApiAuthorService} from '../../service/apiAuthor.service';
import {Article} from '../../model/article';
import {Category} from '../../model/category';
import {NewBodyTag} from '../../model/new-body-tag';
import {NewBodyArticle} from '../../model/new-body-article';
import {DisplayableSingleArticle} from '../../model/displayable-single-article';

@Component({
  selector: 'app-modify-article',
  templateUrl: './modify-article.component.html',
  styleUrls: ['./modify-article.component.css']
})
export class ModifyArticleComponent implements OnInit {

  @Input() idCurrentUser;
  listThisUserDrafts: Article[];
  listThisUserPublished: Article[];
  idDraft = '';
  idPublished = '';
  showDraftArea = false;
  showPublishedArea = false;

  listCategories: Category[];
  title = '';
  overview = '';
  content = '';
  categoryId = '';
  tagString = '';
  listTags: NewBodyTag[] = [];
  selectedArticle: Article;

  showMessageIncomplete = false;
  MessageIncomplete = 'Please insert all required information';
  showMessageSuccess = false;
  MessageSuccess = 'Article Modified';
  showMessageSuccessPublished = false;
  MessageSuccessPublished = 'Article Published';

  constructor(
    private apiArticleService: ApiArticleService,
    private apiAuthorService: ApiAuthorService,
    private apiCategoryService: ApiCategoryService
  ) {
  }

  ngOnInit() {
    this.apiArticleService.getAllDraftsByAuthor(this.idCurrentUser).subscribe(
      answer => {
        this.listThisUserDrafts = answer;
      });
    this.apiArticleService.getAllArticlesPublishedByAuthor(this.idCurrentUser).subscribe(
      answer => {
        this.listThisUserPublished = answer;
      });
    this.apiCategoryService.getAllCategories().subscribe(
      answer => {
        this.listCategories = answer;
      });
  }

  displayModifyDraftArea(id: string) {
    this.idPublished = '';
    this.whiteTextBoxes();
    this.infoTextBoxes(id);
    this.showDraftArea = true;
    this.showPublishedArea = false;
    this.showMessageSuccessPublished = false;
  }

  whiteTextBoxes() {
    this.title = '';
    this.overview = '';
    this.content = '';
    this.categoryId = '';
    this.tagString = '';
  }

  infoTextBoxes(id: string) {
    this.apiArticleService.getCompleteArticle(id).subscribe(
      answer => {
        this.selectedArticle = answer;
        this.title = this.selectedArticle.title;
        this.overview = this.selectedArticle.overview;
        this.content = this.selectedArticle.content;
        this.categoryId = this.selectedArticle.category.categoryId;
        let tagsInDraft = '';
        let counter = 0;
        for (const tagArticleSelected of this.selectedArticle.tagSet) {
          const sizeListTags = this.selectedArticle.tagSet.length;
          counter = counter + 1;
          if (counter === 1) {
            tagsInDraft = tagsInDraft + tagArticleSelected.tagName;
          } else {
            tagsInDraft = tagsInDraft + ', ' + tagArticleSelected.tagName;
          }
        }
        this.tagString = tagsInDraft;
      });
  }

  displayModifyPublishedArea(id: string) {
    this.idDraft = '';
    this.whiteTextBoxes();
    this.infoTextBoxes(id);
    this.showPublishedArea = true;
    this.showDraftArea = false;
    this.showMessageSuccessPublished = false;
  }

  modifyDraft(id: string) {
    if (this.title !== '' || this.overview !== '' || this.content !== '' || this.categoryId !== '') {
      for (const toSeparate of this.tagString.split(',')) {
        this.listTags.push(new NewBodyTag(toSeparate.trim()));
      }
      this.apiArticleService.modifyArticle(id, new NewBodyArticle(this.title, this.overview,
        this.content, this.categoryId, this.idCurrentUser, this.listTags)).subscribe();

      this.showMessageSuccess = true;
      this.showMessageIncomplete = false;
      this.showMessageSuccessPublished = false;
      this.whiteTextBoxes();
    } else {
      this.showMessageIncomplete = true;
      this.showMessageSuccess = false;
      this.showMessageSuccessPublished = false;
    }
  }

  publish(id: string) {
    if (this.content.length > 1) {
      for (const toSeparate of this.tagString.split(',')) {
        this.listTags.push(new NewBodyTag(toSeparate.trim()));
      }
      this.apiArticleService.modifyArticle(id, new NewBodyArticle(this.title, this.overview,
        this.content, this.categoryId, this.idCurrentUser, this.listTags)).subscribe();
      this.apiArticleService.publishArticle(id, 'PUBLISHED').subscribe();

      this.showMessageSuccessPublished = true;
      this.showMessageSuccess = false;
      this.showMessageIncomplete = false;
    } else {
      this.showMessageIncomplete = true;
      this.showMessageSuccess = false;
      this.showMessageSuccessPublished = false;
    }
  }


}
