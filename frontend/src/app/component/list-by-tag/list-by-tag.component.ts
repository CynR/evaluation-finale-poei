import {Component, OnInit} from '@angular/core';
import {ApiArticleService} from '../../service/apiArticle.service';
import {ApiTagService} from '../../service/apiTag.service';
import {Tag} from '../../model/tag';
import {Article} from '../../model/article';

@Component({
  selector: 'app-list-by-tag',
  templateUrl: './list-by-tag.component.html',
  styleUrls: ['./list-by-tag.component.css']
})
export class ListByTagComponent implements OnInit {

  listTags: Tag[];
  listArticlesByTag: Article[];
  tagName = '';

  constructor(
    private apiArticleService: ApiArticleService,
    private apiTagService: ApiTagService
  ) {
  }

  ngOnInit() {
    this.apiTagService.getAllTags().subscribe(
      answer => {
        this.listTags = answer;
      });
  }

  selectTag(id: string) {
    this.apiArticleService.getPubArticlesByTag(id).subscribe(
      answer => {
        this.listArticlesByTag = answer;
      });
  }

}
