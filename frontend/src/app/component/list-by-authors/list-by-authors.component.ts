import {Component, OnInit} from '@angular/core';
import {Article} from '../../model/article';
import {ApiArticleService} from '../../service/apiArticle.service';
import {Author} from '../../model/author';
import {ApiAuthorService} from '../../service/apiAuthor.service';

@Component({
  selector: 'app-list-by-authors',
  templateUrl: './list-by-authors.component.html',
  styleUrls: ['./list-by-authors.component.css']
})
export class ListByAuthorsComponent implements OnInit {

  listAuthors: Author[];
  listArticlesByAuthors: Article[];
  id = '';
  pseudo = '';

  constructor(
    private apiArticleService: ApiArticleService,
    private apiAuthorService: ApiAuthorService
  ) {
  }

  ngOnInit() {
    this.apiAuthorService.getAllAuthors().subscribe(
      answer => {
        this.listAuthors = answer;
      });
  }

  selectAuthor(id: string) {
    this.apiArticleService.getAllArticlesPublishedByAuthor(id).subscribe(
      answer => {
        this.listArticlesByAuthors = answer;
      });
  }

}
