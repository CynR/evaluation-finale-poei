import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListByAuthorsComponent } from './list-by-authors.component';

describe('ListByAuthorsComponent', () => {
  let component: ListByAuthorsComponent;
  let fixture: ComponentFixture<ListByAuthorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListByAuthorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListByAuthorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
