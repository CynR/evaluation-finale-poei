import {Component, OnInit} from '@angular/core';
import {ApiArticleService} from '../../service/apiArticle.service';
import {ApiCategoryService} from '../../service/apiCategory.service';
import {Category} from '../../model/category';
import {Article} from '../../model/article';

@Component({
  selector: 'app-list-by-category',
  templateUrl: './list-by-category.component.html',
  styleUrls: ['./list-by-category.component.css']
})
export class ListByCategoryComponent implements OnInit {

  listCategories: Category[];
  listArticlesByCategory: Article[];
  categoryName = '';

  constructor(
    private apiArticleService: ApiArticleService,
    private apiCategoryService: ApiCategoryService
  ) {
  }

  ngOnInit() {
    this.apiCategoryService.getAllCategories().subscribe(
      answer => {
        this.listCategories = answer;
      });
  }

  selectCategory(id: string) {
    this.apiArticleService.getPubArticlesByCategory(id).subscribe(
      answer => {
        this.listArticlesByCategory = answer;
      });
  }

}
