import {Component, OnInit} from '@angular/core';
import {ApiArticleService} from '../../service/apiArticle.service';
import {Author} from '../../model/author';
import {NewBodyAuthor} from '../../model/new-body-author';
import {ApiAuthorService} from '../../service/apiAuthor.service';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})
export class AuthorComponent implements OnInit {

  pseudo = '';
  mail = '';
  existingPseudo = '';
  existingMail = '';
  showMessageIncorrectInfo = false;
  MessageIncorrectInfo = 'Please insert all required information';
  showMessageSuccess = false;
  MessageSuccess = 'User Created';
  existingMember = false;

  constructor(
    private apiAuthorService: ApiAuthorService
  ) {
  }

  ngOnInit() {
  }

  createUser() {
    if (this.pseudo.length > 0 || this.mail.length > 0) {
      const newUser: NewBodyAuthor = new NewBodyAuthor(this.pseudo, this.mail);
      this.apiAuthorService.createAuthor(newUser).subscribe();
    }
  }

  logIn() {

  }


}
