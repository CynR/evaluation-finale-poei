import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Author} from '../../model/author';
import {ApiArticleService} from '../../service/apiArticle.service';
import {ActivatedRoute} from '@angular/router';
import {ApiAuthorService} from '../../service/apiAuthor.service';
import {Article} from '../../model/article';

@Component({
  selector: 'app-login-home',
  templateUrl: './login-home.component.html',
  styleUrls: ['./login-home.component.css']
})
export class LoginHomeComponent implements OnInit {

  @Input() existingUser: Author;
  @Input() myDrafts: Article[];
  @Output() idAuthor: string;
  displayCreateArea = false;
  displayModifyArea = false;
  displayDeleteArea = false;

  constructor(
    private apiAuthorService: ApiAuthorService,
    private apiArticleService: ApiArticleService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    const userMail = this.route.snapshot.paramMap.get('mail');
    this.apiAuthorService.getByMail(userMail).subscribe(
      answer => {
        this.existingUser = answer;
        this.idAuthor = this.existingUser.id;
        this.apiArticleService.getAllDraftsByAuthor(this.existingUser.id).subscribe(
          drafts => {
            this.myDrafts = drafts;
          });
      });
  }

  showCreateArea() {
    this.displayCreateArea = true;
    this.displayModifyArea = false;
    this.displayDeleteArea = false;
  }

  showModifyArea() {
    this.displayModifyArea = true;
    this.displayCreateArea = false;
    this.displayDeleteArea = false;
  }

  showDeleteArea() {
    this.displayDeleteArea = true;
    this.displayModifyArea = false;
    this.displayCreateArea = false;
  }
}
