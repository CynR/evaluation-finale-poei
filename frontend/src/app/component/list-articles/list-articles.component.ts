import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DisplayableListArticle} from '../../model/displayable-list-article';
import {HttpClient} from '@angular/common/http';
import {ApiArticleService} from '../../service/apiArticle.service';
import {Article} from '../../model/article';
import {ConsultedBody} from '../../model/consulted-body';

@Component({
  selector: 'app-list-articles',
  templateUrl: './list-articles.component.html',
  styleUrls: ['./list-articles.component.css']
})
export class ListArticlesComponent implements OnInit {

  articleList: Article[];

  constructor(
    private apiArticleService: ApiArticleService
  ) {
  }

  ngOnInit() {
    this.apiArticleService.getAllPublishedArticles().subscribe(
      answer => {
        this.articleList = answer;
      });
  }

  changeIsConsultedArticle(id: string) {
    const consulted = true;
    this.apiArticleService.isArticleConsulted(id, new ConsultedBody(consulted))
      .subscribe();
  }
}
