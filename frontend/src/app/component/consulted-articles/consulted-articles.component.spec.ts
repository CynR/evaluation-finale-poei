import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultedArticlesComponent } from './consulted-articles.component';

describe('ConsultedArticlesComponent', () => {
  let component: ConsultedArticlesComponent;
  let fixture: ComponentFixture<ConsultedArticlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultedArticlesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultedArticlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
