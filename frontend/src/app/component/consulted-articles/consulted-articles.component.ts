import {Component, Input, OnInit} from '@angular/core';
import {Article} from '../../model/article';
import {ApiArticleService} from '../../service/apiArticle.service';
import {ConsultedBody} from '../../model/consulted-body';

@Component({
  selector: 'app-consulted-articles',
  templateUrl: './consulted-articles.component.html',
  styleUrls: ['./consulted-articles.component.css']
})
export class ConsultedArticlesComponent implements OnInit {

  listConsultedArts: Article[];

  constructor(
    private apiArticleService: ApiArticleService
  ) {
  }

  ngOnInit() {
    this.apiArticleService.getConsultedArticles()
      .subscribe(answer => {
        this.listConsultedArts = answer;
      });
  }

  eraseHistoricOfConsultedArts() {
    for (const art of this.listConsultedArts) {
      this.apiArticleService.isArticleConsulted(art.id, new ConsultedBody(false))
        .subscribe(() => {
          this.apiArticleService.getConsultedArticles()
            .subscribe(answer => {
              this.listConsultedArts = answer;
            });
        });
    }
  }

}
