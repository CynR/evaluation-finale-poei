export class DisplayableSingleArticle {
  id: string;
  title: string;
  content: string;
  overview: string;
  status: string;
  consulted: boolean;
}
