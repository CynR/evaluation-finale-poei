import {Author} from './author';
import {Tag} from './tag';
import {Category} from './category';

export class Article {
  id: string;
  title: string;
  content: string;
  overview: string;
  status: string;
  category: Category;
  author: Author;
  tagSet: Tag[];
  consulted: boolean;
}
