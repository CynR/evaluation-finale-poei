export class DisplayableListArticle {
  id: string;
  title: string;
  overview: string;
  consulted: boolean;
}
