export class NewBodyTag {
  tagName: string;

  constructor(tagName: string) {
    this.tagName = tagName;
  }
}
