export class NewBodyAuthor {
  pseudo: string;
  mail: string;

  constructor(pseudo: string, mail: string) {
    this.pseudo = pseudo;
    this.mail = mail;
  }
}
