export class ConsultedBody {
  consulted: boolean;

  constructor(consulted: boolean) {
    this.consulted = consulted;
  }
}
