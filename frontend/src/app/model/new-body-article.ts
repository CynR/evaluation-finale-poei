import {Category} from './category';
import {Author} from './author';
import {Tag} from './tag';
import {NewBodyTag} from './new-body-tag';


export class NewBodyArticle {
  title: string;
  overview: string
  content: string;
  categoryId: string;
  authorId: string;
  tags: NewBodyTag[];

  constructor(title: string, overview: string, content: string, categoryId: string, authorId: string, tags: NewBodyTag[]) {
    this.title = title;
    this.overview = overview;
    this.content = content;
    this.categoryId = categoryId;
    this.authorId = authorId;
    this.tags = tags;
  }
}
