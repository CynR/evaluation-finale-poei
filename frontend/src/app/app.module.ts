import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import { ChartsModule } from 'ng2-charts';

import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ArticleComponent} from './component/article/article.component';
import {ListArticlesComponent} from './component/list-articles/list-articles.component';
import {ApiArticleService} from './service/apiArticle.service';
import {AuthorComponent} from './component/author/author.component';
import {CategoryComponent} from './component/category/category.component';
import {TagComponent} from './component/tag/tag.component';
import {HomeComponent} from './component/home/home.component';
import {ArticleDetailComponent} from './component/article-detail/article-detail.component';
import {ApiAuthorService} from './service/apiAuthor.service';
import {LoginHomeComponent} from './component/login-home/login-home.component';
import {ListByAuthorsComponent} from './component/list-by-authors/list-by-authors.component';
import {CreateArticleComponent} from './component/create-article/create-article.component';
import {ModifyArticleComponent} from './component/modify-article/modify-article.component';
import {ApiCategoryService} from './service/apiCategory.service';
import { ListByCategoryComponent } from './component/list-by-category/list-by-category.component';
import { ListByTagComponent } from './component/list-by-tag/list-by-tag.component';
import {ApiTagService} from './service/apiTag.service';
import { StatisticsComponent } from './component/statistics/statistics.component';
import { NumbersComponent } from './component/numbers/numbers.component';
import { SearchByTitleComponent } from './component/search-by-title/search-by-title.component';
import { DeleteArticleComponent } from './component/delete-article/delete-article.component';
import { ConsultedArticlesComponent } from './component/consulted-articles/consulted-articles.component';

const appRoutes: Routes = [
  {path: 'articles', component: HomeComponent},
  {path: 'articles/:id', component: ArticleDetailComponent},
  {path: 'users/:mail', component: LoginHomeComponent},
  {path: 'statistics', component: StatisticsComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ArticleComponent,
    ListArticlesComponent,
    AuthorComponent,
    CategoryComponent,
    TagComponent,
    HomeComponent,
    ArticleDetailComponent,
    LoginHomeComponent,
    ListByAuthorsComponent,
    CreateArticleComponent,
    ModifyArticleComponent,
    ListByCategoryComponent,
    ListByTagComponent,
    StatisticsComponent,
    NumbersComponent,
    SearchByTitleComponent,
    DeleteArticleComponent,
    ConsultedArticlesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    ),
    FormsModule,
    ReactiveFormsModule,
    ChartsModule
  ],
  providers: [
    ApiArticleService,
    ApiAuthorService,
    ApiAuthorService,
    ApiCategoryService,
    ApiTagService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
