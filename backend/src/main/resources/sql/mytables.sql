drop table if exists authors;
create table authors
(
id    text primary key,
pseudo      text not null,
mail        text not null
);

insert into authors
values('33933a16-70a7-4dfb-ac92-f76aa30fbc02',
	  'cyn', 'cyn@mail.com');

CREATE unique INDEX index_author
on authors (mail);

drop table if exists articles;
create table articles
(
id		text primary key,
title 			text not null,
content         text not null,
overview        text not null,
status      	 int not null,
category_id		 text not null,
author_id  		text not null,
isconsulted     boolean
);

drop table if exists categories;
create table categories(
id text primary key,
category_name text not null
);

insert into categories
values('f0a32ac5-5ca3-4532-8380-798e5a3f28b8', 'sport');
insert into categories
values('71995968-32bb-4864-982b-550a637e0e61', 'science');
insert into categories
values('b3b622f6-977c-48fd-96bb-580613122ffd', 'travel');
insert into categories
values('495750d2-5acc-47e4-a588-5caaebf4d1c7', 'beauty');
insert into categories
values('a287a907-d3bf-46c5-92b5-d9b319fd3a57', 'tech');
insert into categories
values('dcb0fa45-8b28-49a8-8b35-32bf1d2b398b', 'food');
insert into categories
values('9ac76191-b1dd-4e6d-96c2-06b106a34b9b', 'nature');
insert into categories
values('49bedcdc-623e-43a8-9c2d-cac4813651c3', 'health');
insert into categories
values('aa329d73-416a-4776-b47c-55a14a2e4c37', 'fashion');
insert into categories
values('8b70b801-ff4b-4aef-a2a1-7547cd101cce', 'cinema');
insert into categories
values('2138eef8-b5a3-485f-82c8-3b3a1965ccca', 'religion');
insert into categories
values('5a321e2f-971a-4462-9683-815899d142e3', 'animals');

drop table if exists tags;
create table tags(
id text primary key,
tag_name text
);

insert into tags
values('eafb04c1-2376-4bd9-a0db-8468c5c26221', 'food');
insert into tags
values('a31e1ca7-1f3b-40d0-8c1f-c672d8df46de', 'chef');
insert into tags
values('f2d50d45-2356-4ca8-9e5a-a734b7fc0767', 'hungry');

drop table if exists article_tags;
create table article_tags
(
id_article    text references articles (id),
id_tag      text references tags (id),
PRIMARY KEY (id_article, id_tag)
);

insert into articles
values('eae2c46c-157e-426e-b9dd-57f8f420917b',
	  'Top Chef', 'Watch it makes me hungry',
	  'Everytime I watch the program i get so hungry',
	  1, 'dcb0fa45-8b28-49a8-8b35-32bf1d2b398b',
	  '33933a16-70a7-4dfb-ac92-f76aa30fbc02',
	  false);

insert into articles
values('bd089331-9be6-4c3f-82e0-cd09c2697cc7',
	  'Tour around Atlixco Atlixco , Puebla, Mexico',
	   'Under the blue sky, across the valley and mountainous landscapes, find out why Atlixco’s 100,000+ inhabitants insist the town has the best climate in the world, and refer to their home as the city of flowers.',
	  'Under the blue sky, across the valley and mountainous landscapes, find out why Atlixco’s 100,000+ inhabitants insist the town has the best climate in the world, and refer to their home as the city of flowers.',
	  1, 'b3b622f6-977c-48fd-96bb-580613122ffd',
	  '33933a16-70a7-4dfb-ac92-f76aa30fbc02',
	  false);

insert into articles
values('00a4e4fa-2bdc-40ce-a3d7-2869c883dc56',
	  'Visit the Lagoon of Seven Colors in Bacalar Bacalar , Quintana Roo, Mexico',
	   'The lagoon extends quietly and picturesquely next to the village of Bacalar as a perfect backdrop for adventure and nature lovers.',
	  'The lagoon extends quietly and picturesquely next to the village of Bacalar as a perfect backdrop for adventure and nature lovers.',
	  1, 'b3b622f6-977c-48fd-96bb-580613122ffd',
      	  '33933a16-70a7-4dfb-ac92-f76aa30fbc02',
      	  false);

insert into articles
values('b23b13bc-9913-45d9-8107-a0965dfb53a3',
	  'Be amazed by the Popocatépetl Volcano Cholula , Puebla, Mexico',
	   'This imposing and majestic volcano crowned with snow occasionally blows puffs of smoke to remind the world that its spirit still endures.',
	  'This imposing and majestic volcano crowned with snow occasionally blows puffs of smoke to remind the world that its spirit still endures.',
	  0, 'b3b622f6-977c-48fd-96bb-580613122ffd',
      	  '33933a16-70a7-4dfb-ac92-f76aa30fbc02',
      	  false);

insert into articles
values('97db840f-7d70-436d-936a-0fc3b1f9966e',
	  'Admire the plants at the Francisco Clavijero Botanical Garden Xalapa , Veracruz, Mexico',
	  'Review the program of activities. You will be able to participate in a variety of activities with your family, such as workshops, scientific camps, thematic tours, cinema cycles and certificate courses.',
	  'Review the program of activities. You will be able to participate in a variety of activities with your family, such as workshops, scientific camps, thematic tours, cinema cycles and certificate courses.',
	  1, 'b3b622f6-977c-48fd-96bb-580613122ffd',
      	  '33933a16-70a7-4dfb-ac92-f76aa30fbc02',
      	  false);

insert into article_tags
values('eae2c46c-157e-426e-b9dd-57f8f420917b', 'eafb04c1-2376-4bd9-a0db-8468c5c26221');
insert into article_tags
values('eae2c46c-157e-426e-b9dd-57f8f420917b', 'a31e1ca7-1f3b-40d0-8c1f-c672d8df46de');
insert into article_tags
values('eae2c46c-157e-426e-b9dd-57f8f420917b', 'f2d50d45-2356-4ca8-9e5a-a734b7fc0767');