package com.cyn.backend.domain;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "articles")
public class Article {
    @Id
    private String id;
    // if status PUBLISHED => not modifiable
    private String title;
    private String content;
    // max 50 words
    // if status PUBLISHED => not modifiable
    private String overview;
    private StatusArticle status;
    // just 1 category by article
    @ManyToOne
    @JoinColumn(name = "categoryId")
    private Category category;
    // not modifiable
    @ManyToOne
    @JoinColumn(name = "authorId")
    private Author author;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "article_tags",
            joinColumns = @JoinColumn(name = "id_article"),
            inverseJoinColumns = @JoinColumn(name = "id_tag")
    )
    private Set<Tag> tagSet;
    private Boolean isconsulted;

    public Article(String id, String title, String content, String overview,
                   StatusArticle status, Category category,
                   Author author, Set<Tag> tagSet, Boolean isconsulted) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.overview = overview;
        this.status = status;
        this.category = category;
        this.author = author;
        this.tagSet = tagSet;
        this.isconsulted = isconsulted;
    }

    protected Article() {}

    @Override
    public String toString() {
        return "Article{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", overview='" + overview + '\'' +
                ", status=" + status +
                ", category=" + category +
                ", author=" + author +
                ", tagSet=" + tagSet +
                ", isconsulted=" + isconsulted +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public StatusArticle getStatus() {
        return status;
    }

    public void setStatus(StatusArticle status) {
        this.status = status;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Set<Tag> getTagSet() {
        return tagSet;
    }

    public void setTagSet(Set<Tag> tagSet) {
        this.tagSet = tagSet;
    }

    public Boolean getConsulted() {
        return isconsulted;
    }

    public void setConsulted(Boolean consulted) {
        isconsulted = consulted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Article)) return false;
        Article article = (Article) o;
        return Objects.equals(getId(), article.getId()) &&
                Objects.equals(getTitle(), article.getTitle()) &&
                Objects.equals(getContent(), article.getContent()) &&
                Objects.equals(getOverview(), article.getOverview()) &&
                getStatus() == article.getStatus() &&
                Objects.equals(getCategory(), article.getCategory()) &&
                Objects.equals(getAuthor(), article.getAuthor()) &&
                Objects.equals(getTagSet(), article.getTagSet()) &&
                Objects.equals(isconsulted, article.isconsulted);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle(), getContent(), getOverview(), getStatus(), getCategory(), getAuthor(), getTagSet(), isconsulted);
    }
}
