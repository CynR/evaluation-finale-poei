package com.cyn.backend.domain;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "authors")
public class Author {
    @Id
    @Column(name = "id")
    private String authorId;
    private String pseudo;
    private String mail;

    @OneToMany(mappedBy = "author")
    private List<Article> articles;

    public Author(String authorId, String pseudo, String mail) {
        this.authorId = authorId;
        this.pseudo = pseudo;
        this.mail = mail;
    }

    public Author() {}

    @Override
    public String toString() {
        return "Author{" +
                "authorId='" + authorId + '\'' +
                ", pseudo='" + pseudo + '\'' +
                ", mail='" + mail + '\'' +
                '}';
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Author)) return false;
        Author author = (Author) o;
        return Objects.equals(getAuthorId(), author.getAuthorId()) &&
                Objects.equals(getPseudo(), author.getPseudo()) &&
                Objects.equals(getMail(), author.getMail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAuthorId(), getPseudo(), getMail());
    }
}
