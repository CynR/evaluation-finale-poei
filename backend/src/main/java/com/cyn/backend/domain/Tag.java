package com.cyn.backend.domain;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "tags")
public class Tag {
    @Id
    private String id;
    private String tagName;

    @ManyToMany(mappedBy = "tagSet")
    Set<Article> articles;

    public Tag(String id, String tagName) {
        this.id = id;
        this.tagName = tagName;
    }

    public Tag() {}

    @Override
    public String toString() {
        return "Tag{" +
                "id='" + id + '\'' +
                ", tagName='" + tagName + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tag)) return false;
        Tag tag = (Tag) o;
        return Objects.equals(getId(), tag.getId()) &&
                Objects.equals(getTagName(), tag.getTagName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTagName());
    }
}
