package com.cyn.backend.domain;

public enum StatusArticle {
    DRAFT,
    PUBLISHED
}
