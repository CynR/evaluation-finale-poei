package com.cyn.backend.repositories;

import com.cyn.backend.domain.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, String> {
}
