package com.cyn.backend.repositories;

import com.cyn.backend.domain.Author;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AuthorRepository extends CrudRepository<Author, String> {

    Optional<Author> findByMail(String mail);
}
