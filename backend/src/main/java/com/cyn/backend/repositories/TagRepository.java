package com.cyn.backend.repositories;

import com.cyn.backend.domain.Tag;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TagRepository extends CrudRepository<Tag, String> {

    Optional<Tag> findByTagName(String tagName);
}
