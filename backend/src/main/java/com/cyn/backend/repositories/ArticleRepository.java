package com.cyn.backend.repositories;

import com.cyn.backend.domain.Article;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ArticleRepository extends CrudRepository<Article, String> {

    public Optional<Article> findByTitle(String title);
}
