package com.cyn.backend.controllers.representation.article;

import com.cyn.backend.domain.Article;
import org.springframework.stereotype.Component;

@Component
public class ArticleMapperListArticles {

    public DisplayableArticlesListRep mapToListArticlesRepresentation(Article article) {
        DisplayableArticlesListRep result = new DisplayableArticlesListRep();
        result.setId(article.getId());
        result.setTitle(article.getTitle());
        result.setOverview(article.getOverview());
        result.setConsulted(article.getConsulted());
        return result;
    }

}
