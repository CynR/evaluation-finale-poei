package com.cyn.backend.controllers.representation.tag;

public class NewTagBody {
    private String tagName;

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
