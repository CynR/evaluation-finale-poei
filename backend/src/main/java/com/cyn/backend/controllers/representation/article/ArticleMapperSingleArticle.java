package com.cyn.backend.controllers.representation.article;

import com.cyn.backend.domain.Article;
import org.springframework.stereotype.Component;

@Component
public class ArticleMapperSingleArticle {

    public DisplayableSingleArticleRep mapToSingleArticleRep(Article article) {
        DisplayableSingleArticleRep result = new DisplayableSingleArticleRep();
        result.setId(article.getId());
        result.setTitle(article.getTitle());
        result.setOverview(article.getOverview());
        result.setContent(article.getContent());
        result.setStatus(article.getStatus());
        result.setConsulted(article.getConsulted());
        return result;
    }

}
