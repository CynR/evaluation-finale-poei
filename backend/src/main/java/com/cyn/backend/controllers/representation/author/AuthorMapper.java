package com.cyn.backend.controllers.representation.author;

import com.cyn.backend.domain.Author;
import org.springframework.stereotype.Component;

@Component
public class AuthorMapper {
    public DisplayableAuthorRep mapAuthor(Author author) {
        DisplayableAuthorRep result = new DisplayableAuthorRep();
        result.setId(author.getAuthorId());
        result.setPseudo(author.getPseudo());
        result.setMail(author.getMail());
        return result;
    }
}
