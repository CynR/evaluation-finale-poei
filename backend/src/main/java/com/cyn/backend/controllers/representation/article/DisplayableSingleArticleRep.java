package com.cyn.backend.controllers.representation.article;

import com.cyn.backend.domain.StatusArticle;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DisplayableSingleArticleRep {
    private String id;
    private String title;
    private String overview;
    private String content;
    private StatusArticle status;
    private Boolean isConsulted;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public StatusArticle getStatus() {
        return status;
    }

    public void setStatus(StatusArticle status) {
        this.status = status;
    }

    public Boolean getConsulted() {
        return isConsulted;
    }

    public void setConsulted(Boolean consulted) {
        isConsulted = consulted;
    }
}
