package com.cyn.backend.controllers;

import com.cyn.backend.controllers.representation.author.AuthorMapper;
import com.cyn.backend.controllers.representation.author.DisplayableAuthorRep;
import com.cyn.backend.controllers.representation.author.NewAuthorBody;
import com.cyn.backend.domain.Author;
import com.cyn.backend.services.AuthorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class AuthorController {
    private AuthorService authorService;
    private AuthorMapper mapper;

    public AuthorController(AuthorService authorService, AuthorMapper mapper) {
        this.authorService = authorService;
        this.mapper = mapper;
    }

    @GetMapping("/{mail}")
    public ResponseEntity<DisplayableAuthorRep> getByMail(@PathVariable("mail") String mail) {
        Optional<Author> author = this.authorService.getByMail(mail);
        return author
                .map(this.mapper::mapAuthor)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/all")
    public List<DisplayableAuthorRep> getAllAuthors() {
        return this.authorService.getAllAuthors().stream()
                .map(this.mapper::mapAuthor)
                .collect(Collectors.toList());
    }

    @PostMapping
    public ResponseEntity<DisplayableAuthorRep> createUser(@RequestBody NewAuthorBody body) {
        Optional<Author> author = this.authorService.createAuthor(body);
        return author
                .map(this.mapper::mapAuthor)
                .map(ResponseEntity :: ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public Boolean userExists(@RequestBody NewAuthorBody body) {
        return this.authorService.doesAuthorExists(body.getMail());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<DisplayableAuthorRep> deleteAuthor(@PathVariable("id") String id) {
        Optional<Author> author = this.authorService.deleteAuthor(id);
        return author
                .map(this.mapper::mapAuthor)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }


}
