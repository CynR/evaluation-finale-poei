package com.cyn.backend.controllers;

import com.cyn.backend.controllers.representation.category.CategoryMapper;
import com.cyn.backend.controllers.representation.category.DisplayableCategoryRep;
import com.cyn.backend.domain.Category;
import com.cyn.backend.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/categories")
public class CategoryController {
    private CategoryService categoryService;
    private CategoryMapper mapper;

    @Autowired
    public CategoryController(CategoryService categoryService, CategoryMapper mapper) {
        this.categoryService = categoryService;
        this.mapper = mapper;
    }

    @GetMapping
    public List<DisplayableCategoryRep> getAllCategories() {
        return this.categoryService.getAllCategories().stream()
                .map(this.mapper::mapCategory)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<DisplayableCategoryRep> getCategoryById(@PathVariable("id") String id) {
        Optional<Category> category = this.categoryService.getById(id);
        return category
                .map(this.mapper::mapCategory)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<DisplayableCategoryRep> deleteCategory(@PathVariable("id") String id) {
        Optional<Category> category = this.categoryService.deleteCategory(id);
        return category
                .map(this.mapper::mapCategory)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
