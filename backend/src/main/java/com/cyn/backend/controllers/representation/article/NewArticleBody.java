package com.cyn.backend.controllers.representation.article;

import com.cyn.backend.controllers.representation.tag.NewTagBody;

import java.util.Set;

public class NewArticleBody {
    private String title;
    private String overview;
    private String content;
    private String categoryId;
    private String authorId;
    private Set<NewTagBody> tags;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public Set<NewTagBody> getTags() {
        return tags;
    }

    public void setTags(Set<NewTagBody> tags) {
        this.tags = tags;
    }
}
