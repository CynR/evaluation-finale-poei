package com.cyn.backend.controllers;

import com.cyn.backend.controllers.representation.article.*;
import com.cyn.backend.domain.Article;
import com.cyn.backend.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/articles")
public class ArticleController {
    private ArticleService articleService;
    private ArticleMapperListArticles mapperListArticles;
    private ArticleMapperSingleArticle mapperSingleArticle;

    @Autowired
    public ArticleController(ArticleService articleService, ArticleMapperListArticles mapperListArticles, ArticleMapperSingleArticle mapperSingleArticle) {
        this.articleService = articleService;
        this.mapperListArticles = mapperListArticles;
        this.mapperSingleArticle = mapperSingleArticle;
    }

    @GetMapping
    List<DisplayableArticlesListRep> getAllPublishedArticles() {
        return this.articleService.getAllPublishedArticles().stream()
                .map(this.mapperListArticles::mapToListArticlesRepresentation)
                .collect(Collectors.toList());
    }

    @GetMapping("/drafts")
    List<DisplayableArticlesListRep> getAllDraftArticles() {
        return this.articleService.getAllDraftsArticles().stream()
                .map(this.mapperListArticles::mapToListArticlesRepresentation)
                .collect(Collectors.toList());
    }

    @GetMapping("/all")
    List<DisplayableArticlesListRep> getAllListOfArticles() {
        return this.articleService.getAllArticles().stream()
                .map(this.mapperListArticles::mapToListArticlesRepresentation)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<DisplayableSingleArticleRep> getOneArticle(@PathVariable("id") String id) {
        Optional<Article> article = this.articleService.getById(id);
        return article
                .map(this.mapperSingleArticle::mapToSingleArticleRep)
                .map(ResponseEntity :: ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/complete/{id}")
    public ResponseEntity<Article> getCompleteArticle(@PathVariable("id") String id) {
        Optional<Article> article = this.articleService.getById(id);
        return article
                .map(ResponseEntity :: ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    }

    @GetMapping("/drafts/{id}")
    public List<DisplayableArticlesListRep> getAllDraftsByIdAuthor(@PathVariable("id") String idAuthor) {
        return this.articleService.getDraftsByAuthor(idAuthor).stream()
                .map(this.mapperListArticles::mapToListArticlesRepresentation)
                .collect(Collectors.toList());
    }

    @GetMapping("/published/{id}")
    public List<DisplayableArticlesListRep> getPublishedArticlesByIdAuthor(@PathVariable("id") String idAuthor) {
        return this.articleService.getPublishedByAuthor(idAuthor).stream()
                .map(this.mapperListArticles::mapToListArticlesRepresentation)
                .collect(Collectors.toList());
    }

    @GetMapping("/all/{id}")
    public List<DisplayableArticlesListRep> getAllArticlesByIdAuthor(@PathVariable("id") String idAuthor) {
        return this.articleService.getAllArticlesByAuthor(idAuthor).stream()
                .map(this.mapperListArticles::mapToListArticlesRepresentation)
                .collect(Collectors.toList());
    }

    @GetMapping("/published/category/{id}")
    public List<DisplayableArticlesListRep> getPublishedArticlesByCategory(@PathVariable("id") String idCategory) {
        return this.articleService.getPublishedArticledByCategory(idCategory).stream()
                .map(this.mapperListArticles::mapToListArticlesRepresentation)
                .collect(Collectors.toList());
    }

    @GetMapping("/drafts/category/{id}")
    public List<DisplayableArticlesListRep> getDraftsByCategory(@PathVariable("id") String idCategory) {
        return this.articleService.getDraftsByCategory(idCategory).stream()
                .map(this.mapperListArticles::mapToListArticlesRepresentation)
                .collect(Collectors.toList());
    }
    @GetMapping("/all/category/{id}")
    public List<DisplayableArticlesListRep> getAllArticlesByCategory(@PathVariable("id") String idCategory) {
        return this.articleService.getAllArticlesByCategory(idCategory).stream()
                .map(this.mapperListArticles::mapToListArticlesRepresentation)
                .collect(Collectors.toList());
    }

    @GetMapping("/published/tag/{id}")
    public List<DisplayableArticlesListRep> getPublishedArticlesByTag(@PathVariable("id") String idTag) {
        return this.articleService.getPublishedArticlesByTag(idTag).stream()
                .map(this.mapperListArticles::mapToListArticlesRepresentation)
                .collect(Collectors.toList());
    }

    @GetMapping("/drafts/tag/{id}")
    public List<DisplayableArticlesListRep> getDraftsByTag(@PathVariable("id") String idTag) {
        return this.articleService.getDraftsByTag(idTag).stream()
                .map(this.mapperListArticles::mapToListArticlesRepresentation)
                .collect(Collectors.toList());
    }

    @GetMapping("/all/tag/{id}")
    public List<DisplayableArticlesListRep> getAllArticlesByTag(@PathVariable("id") String idTag) {
        return this.articleService.getAllArticlesByTag(idTag).stream()
                .map(this.mapperListArticles::mapToListArticlesRepresentation)
                .collect(Collectors.toList());
    }

    @GetMapping("all/consulted")
    List<DisplayableArticlesListRep> getAllConsultedArticles() {
        return this.articleService.getAllConsultedArticles().stream()
                .map(this.mapperListArticles::mapToListArticlesRepresentation)
                .collect(Collectors.toList());
    }

    @PostMapping
    public ResponseEntity<Article> createArticle(@RequestBody NewArticleBody body) {
        Optional<Article> article = this.articleService.createArticle(body);
        return article
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.noContent().build());
    }

    @PutMapping("/content/{id}")
    public ResponseEntity<DisplayableSingleArticleRep> modifyPublishedArticle(@PathVariable("id") String id,
                                                          @RequestBody String newContent) {
        Optional<Article> article = this.articleService.modifyArticleContent(id, newContent);
        return article
                .map(this.mapperSingleArticle::mapToSingleArticleRep)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/status/{id}")
    public ResponseEntity<DisplayableSingleArticleRep> publishArticle(@PathVariable("id") String id,
                                                            @RequestBody String published) {
        Optional<Article> article = this.articleService.publishArticle(id, published);
        return article
                .map(this.mapperSingleArticle::mapToSingleArticleRep)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/consulted/{id}")
    public ResponseEntity<DisplayableSingleArticleRep> changeIsConsulted(@PathVariable("id") String id,
                                                                      @RequestBody ConsultedBody isConsulted) {
        Optional<Article> article = this.articleService.isArticleConsulted(id, isConsulted);
        return article
                .map(this.mapperSingleArticle::mapToSingleArticleRep)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<DisplayableSingleArticleRep> modifyArticle(@PathVariable("id") String id,
                                                                    @RequestBody NewArticleBody newBody) {
        Optional<Article> article = this.articleService.modifyArticle(id,newBody);
        return article
                .map(this.mapperSingleArticle::mapToSingleArticleRep)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    // verify - with GetMapping is not possible to recuperate from angular
    @PostMapping("/title")
    public ResponseEntity<DisplayableSingleArticleRep> getOneArticleByTitle(@RequestBody String titleSearched) {
        Optional<Article> article = this.articleService.searchArticleByTitle(titleSearched);
        return article
                .map(this.mapperSingleArticle::mapToSingleArticleRep)
                .map(ResponseEntity :: ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<DisplayableSingleArticleRep> deleteArticle(@PathVariable("id") String id) {
        Optional<Article> article = this.articleService.deleteArticle(id);
        return article
                .map(this.mapperSingleArticle::mapToSingleArticleRep)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
