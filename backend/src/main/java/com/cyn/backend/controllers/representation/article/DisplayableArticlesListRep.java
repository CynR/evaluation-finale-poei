package com.cyn.backend.controllers.representation.article;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DisplayableArticlesListRep {
    private String id;
    private String title;
    private String overview;
    private Boolean isConsulted;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public Boolean getConsulted() {
        return isConsulted;
    }

    public void setConsulted(Boolean consulted) {
        isConsulted = consulted;
    }
}
