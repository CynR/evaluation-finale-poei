package com.cyn.backend.controllers.representation.tag;

import com.cyn.backend.domain.Tag;
import org.springframework.stereotype.Component;

@Component
public class TagMapper {

    public DisplayableTagRep mapTag(Tag tag) {
        DisplayableTagRep result = new DisplayableTagRep();
        result.setId(tag.getId());
        result.setTagName(tag.getTagName());
        return result;
    }
}
