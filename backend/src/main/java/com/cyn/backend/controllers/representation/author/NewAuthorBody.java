package com.cyn.backend.controllers.representation.author;

public class NewAuthorBody {
    private String pseudo;
    private String mail;

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
