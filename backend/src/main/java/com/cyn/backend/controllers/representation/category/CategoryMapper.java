package com.cyn.backend.controllers.representation.category;

import com.cyn.backend.domain.Category;
import org.springframework.stereotype.Component;

@Component
public class CategoryMapper {

    public DisplayableCategoryRep mapCategory(Category category) {
        DisplayableCategoryRep result = new DisplayableCategoryRep();
        result.setCategoryId(category.getCategoryId());
        result.setCategoryName(category.getCategoryName());
        return result;
    }


}
