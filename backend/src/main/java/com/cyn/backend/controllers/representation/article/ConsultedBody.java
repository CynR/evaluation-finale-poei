package com.cyn.backend.controllers.representation.article;

public class ConsultedBody {
    private boolean consulted;

    public boolean getConsulted() {
        return consulted;
    }

    public void setConsulted(boolean consulted) {
        this.consulted = consulted;
    }
}
