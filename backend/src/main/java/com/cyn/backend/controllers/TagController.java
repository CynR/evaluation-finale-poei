package com.cyn.backend.controllers;

import com.cyn.backend.controllers.representation.tag.DisplayableTagRep;
import com.cyn.backend.controllers.representation.tag.TagMapper;
import com.cyn.backend.domain.Tag;
import com.cyn.backend.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/tags")
public class TagController {
    private TagService tagService;
    private TagMapper mapper;

    @Autowired
    public TagController(TagService tagService, TagMapper mapper) {
        this.tagService = tagService;
        this.mapper = mapper;
    }

    @GetMapping
    public List<DisplayableTagRep> getAllTags() {
        return this.tagService.getAllTags().stream()
                .map(this.mapper::mapTag)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<DisplayableTagRep> getTagById(@PathVariable("id") String id) {
        Optional<Tag> tag = this.tagService.getOneTagById(id);
        return tag
                .map(this.mapper::mapTag)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<DisplayableTagRep> deleteTag(@PathVariable("id") String id) {
        Optional<Tag> tag = this.tagService.deleteTag(id);
        return tag
                .map(this.mapper::mapTag)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

}
