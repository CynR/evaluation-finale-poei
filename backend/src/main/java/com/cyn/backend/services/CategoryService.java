package com.cyn.backend.services;

import com.cyn.backend.domain.Category;
import com.cyn.backend.repositories.CategoryRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Component
public class CategoryService {
    private CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public List<Category> getAllCategories() {
        return (List<Category>) this.categoryRepository.findAll();
    }

    public Optional<Category> getById(String id) {
        Optional<Category> category = this.categoryRepository.findById(id);
        if (category.isPresent()){
            return category;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Category not found");
        }

    }

    public Optional<Category> deleteCategory(String id) {
        Optional<Category> cat = this.categoryRepository.findById(id);
        if (cat.isPresent()) {
            this.categoryRepository.deleteById(id);
            return cat;
        } else {
            throw  new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Unsuccessful delete, the id = %s does not correspond to any Category", id));
        }
    }



}
