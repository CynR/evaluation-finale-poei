package com.cyn.backend.services;

import com.cyn.backend.controllers.representation.article.ConsultedBody;
import com.cyn.backend.controllers.representation.article.NewArticleBody;
import com.cyn.backend.controllers.representation.tag.NewTagBody;
import com.cyn.backend.domain.*;
import com.cyn.backend.repositories.ArticleRepository;
import com.cyn.backend.repositories.AuthorRepository;
import com.cyn.backend.repositories.CategoryRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.*;

@Component
public class ArticleService {
    private ArticleRepository articleRepository;
    private AuthorRepository authorRepository;
    private CategoryRepository categoryRepository;
    private TagService tagService;
    private IdGenerator idGenerator;
    private LevenshteinDistance distance;

    public ArticleService(ArticleRepository articleRepository, AuthorRepository authorRepository,
                          CategoryRepository categoryRepository, IdGenerator idGenerator,
                          TagService tagService, LevenshteinDistance distance) {
        this.articleRepository = articleRepository;
        this.authorRepository = authorRepository;
        this.categoryRepository = categoryRepository;
        this.tagService = tagService;
        this.idGenerator = idGenerator;
        this.distance = distance;
    }

    public Optional<Article> getById(String id) {
        Optional<Article> article = this.articleRepository.findById(id);
        if (article.isPresent()) {
            return article;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Article with id = %s does not exist", id));
        }
    }

    public List<Article> getAllDraftsArticles() {
        List<Article> newList = (List<Article>) this.articleRepository.findAll();
        List<Article> listWithoutDrafts = new ArrayList<>();
        for (Article article: newList) {
            if (article.getStatus().equals(StatusArticle.DRAFT)) {
                listWithoutDrafts.add(article);
            }
        }
        return listWithoutDrafts;
    }

    public List<Article> getAllPublishedArticles() {
        List<Article> newList = (List<Article>) this.articleRepository.findAll();
        List<Article> listWithoutDrafts = new ArrayList<>();
        for (Article article: newList) {
            if (article.getStatus().equals(StatusArticle.PUBLISHED)) {
                listWithoutDrafts.add(article);
            }
        }
        return listWithoutDrafts;
    }

    public List<Article> getAllArticles() {
        return (List<Article>) this.articleRepository.findAll();
    }

    public List<Article> getDraftsByAuthor(String idAuthor) {
        List<Article> newList = (List<Article>) this.articleRepository.findAll();
        List<Article> listOfDrafts = new ArrayList<>();
        for (Article article: newList) {
            if (article.getStatus().equals(StatusArticle.DRAFT)) {
                if (article.getAuthor().getAuthorId().equals(idAuthor)) {
                    listOfDrafts.add(article);
                }
            }
        }
        return listOfDrafts;
    }

    public List<Article> getPublishedByAuthor(String idAuthor) {
            List<Article> newList = (List<Article>) this.articleRepository.findAll();
            List<Article> listOfPublished = new ArrayList<>();
            for (Article article: newList) {
                if (article.getStatus().equals(StatusArticle.PUBLISHED)) {
                    if (article.getAuthor().getAuthorId().equals(idAuthor)) {
                        listOfPublished.add(article);
                    }
                }
            }
            return listOfPublished;
    }

    public List<Article> getAllArticlesByAuthor(String idAuthor) {
        List<Article> newList = (List<Article>) this.articleRepository.findAll();
        List<Article> listOfPublished = new ArrayList<>();
        for (Article article: newList) {
            if (article.getAuthor().getAuthorId().equals(idAuthor)) {
                listOfPublished.add(article);
            }
        }
        return listOfPublished;
    }

    public List<Article> getPublishedArticledByCategory(String idCategory) {
        List<Article> newList = (List<Article>) this.articleRepository.findAll();
        List<Article> listPublishedByCategory = new ArrayList<>();
        for (Article article: newList) {
            if (article.getStatus().equals(StatusArticle.PUBLISHED)) {
                if (article.getCategory().getCategoryId().equals(idCategory)) {
                    listPublishedByCategory.add(article);
                }
            }
        }
        return listPublishedByCategory;
    }

    public List<Article> getDraftsByCategory(String idCategory) {
        List<Article> newList = (List<Article>) this.articleRepository.findAll();
        List<Article> listPublishedByCategory = new ArrayList<>();
        for (Article article: newList) {
            if (article.getStatus().equals(StatusArticle.DRAFT)) {
                if (article.getCategory().getCategoryId().equals(idCategory)) {
                    listPublishedByCategory.add(article);
                }
            }
        }
        return listPublishedByCategory;
    }

    public List<Article> getAllArticlesByCategory(String idCategory) {
        List<Article> newList = (List<Article>) this.articleRepository.findAll();
        List<Article> listPublishedByCategory = new ArrayList<>();
        for (Article article: newList) {
            if (article.getCategory().getCategoryId().equals(idCategory)) {
                listPublishedByCategory.add(article);
            }
        }
        return listPublishedByCategory;
    }

    public List<Article> getPublishedArticlesByTag(String idTag) {
        List<Article> newList = (List<Article>) this.articleRepository.findAll();
        List<Article> listPublishedByCategory = new ArrayList<>();
        Tag chosenTag = this.tagService.getOneTagById(idTag).get();
        for (Article article: newList) {
            if (article.getStatus().equals(StatusArticle.PUBLISHED)) {
                for (Tag lookingForTag: article.getTagSet()) {
                    if (lookingForTag.getId().equals(idTag)) {
                        listPublishedByCategory.add(article);
                        break;
                    }
                }
            }
        }
        return listPublishedByCategory;
    }

    public List<Article> getDraftsByTag(String idTag) {
        List<Article> newList = (List<Article>) this.articleRepository.findAll();
        List<Article> listPublishedByCategory = new ArrayList<>();
        Tag chosenTag = this.tagService.getOneTagById(idTag).get();
        for (Article article: newList) {
            if (article.getStatus().equals(StatusArticle.DRAFT)) {
                for (Tag lookingForTag: article.getTagSet()) {
                    if (lookingForTag.getId().equals(idTag)) {
                        listPublishedByCategory.add(article);
                        break;
                    }
                }
            }
        }
        return listPublishedByCategory;
    }

    public List<Article> getAllArticlesByTag(String idTag) {
        List<Article> newList = (List<Article>) this.articleRepository.findAll();
        List<Article> listPublishedByCategory = new ArrayList<>();
        Tag chosenTag = this.tagService.getOneTagById(idTag).get();
        for (Article article: newList) {
            for (Tag lookingForTag: article.getTagSet()) {
                if (lookingForTag.getId().equals(idTag)) {
                    listPublishedByCategory.add(article);
                    break;
                }
            }
        }
        return listPublishedByCategory;
    }

    public List<Article> getAllConsultedArticles() {
        List<Article> newList = (List<Article>) this.articleRepository.findAll();
        List<Article> listConsultedArticles = new ArrayList<>();
        for (Article article: newList) {
            if (article.getConsulted()) {
                listConsultedArticles.add(article);
            }
        }
        return listConsultedArticles;
    }

    @Transactional(rollbackOn = Exception.class)
    public Optional<Article> createArticle (NewArticleBody body) {
        if ( (!body.getTitle().equals("")) || (!body.getOverview().equals("")) || (!body.getContent().equals(""))) {
            final String[] wordsOverview = body.getOverview().split("\\s+");
            if(wordsOverview.length < 51) {
                String idArticle = this.idGenerator.generateNewId();
                Author author = this.authorRepository.findById(body.getAuthorId()).get();
                Category category = this.categoryRepository.findById(body.getCategoryId()).get();
                //check tags
                Set<NewTagBody> listNewTags = body.getTags();
                Set<Tag> listToImplement = new HashSet<>();;
                for (NewTagBody tag: listNewTags) {
                    String isNewTag = tag.getTagName().toLowerCase();
                    if (!this.tagService.doesTagExists(isNewTag)) {
                        Tag tagToSave = new Tag(this.idGenerator.generateNewId(), isNewTag);
                        this.tagService.saveTag(tagToSave);
                        listToImplement.add(tagToSave);
                    } else {
                        listToImplement.add(new Tag
                                (this.tagService.getTagByName(isNewTag).get().getId(), isNewTag));
                    }
                }

                Article article = new Article(
                        idArticle,
                        body.getTitle(),
                        body.getContent(),
                        body.getOverview(),
                        StatusArticle.DRAFT,
                        category,
                        author,
                        listToImplement,
                        false
                );
                this.articleRepository.save(article);
                return Optional.of(article);
            } else {
                throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Unsuccessful creation. The overview can contain maximum 50 words");
            }
            //finishing 50 words OVERVIEW condition
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unsuccessful creation. Please insert valid and complete information");
    }


    public Optional<Article> modifyArticleContent(String id, String newContent) {
        Optional<Article> articleToModify = this.articleRepository.findById(id);
        if (articleToModify.isPresent()) {
            articleToModify.get().setContent(newContent);
            this.articleRepository.save(articleToModify.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Article not exists");
        }
        return articleToModify;
    }

    public Optional<Article> modifyArticle(String id, NewArticleBody newBody) {
        Optional<Article> articleToModify = this.articleRepository.findById(id);
        if (articleToModify.isPresent()) {
            if (!articleToModify.get().getTitle().equals("")) {
                articleToModify.get().setTitle(newBody.getTitle());
            }
            if (!articleToModify.get().getOverview().equals("")) {
                articleToModify.get().setOverview(newBody.getOverview());
            }
            if (!articleToModify.get().getContent().equals("")) {
                articleToModify.get().setContent(newBody.getContent());
            }
            if (!articleToModify.get().getCategory().getCategoryName().equals("")) {
                articleToModify.get().setCategory
                        (this.categoryRepository.findById(newBody.getCategoryId()).get());
            }

            Set<NewTagBody> listNewTags = newBody.getTags();
            Set<Tag> listToImplement = new HashSet<>();;
            for (NewTagBody tag: listNewTags) {
                String isNewTag = tag.getTagName().toLowerCase();
                if (!this.tagService.doesTagExists(isNewTag)) {
                    Tag tagToSave = new Tag(this.idGenerator.generateNewId(), isNewTag);
                    this.tagService.saveTag(tagToSave);
                    listToImplement.add(tagToSave);
                } else {
                    listToImplement.add(new Tag
                            (this.tagService.getTagByName(isNewTag).get().getId(), isNewTag));
                }
            }
            articleToModify.get().setTagSet(listToImplement);

            this.articleRepository.save(articleToModify.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Article not exists");
        }
        return articleToModify;
    }

    public Optional<Article> publishArticle(String id, String published) {
        Optional<Article> articleToModify = this.articleRepository.findById(id);
        if (articleToModify.isPresent()) {
            if (published.equals("PUBLISHED")) {
                articleToModify.get().setStatus(StatusArticle.PUBLISHED);
            }
            this.articleRepository.save(articleToModify.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Article not exists");
        }
        return articleToModify;
    }

    public Optional<Article> isArticleConsulted(String id, ConsultedBody isConsulted) {
        Optional<Article> articleToModify = this.articleRepository.findById(id);
        if (articleToModify.isPresent()) {
            articleToModify.get().setConsulted(isConsulted.getConsulted());
            this.articleRepository.save(articleToModify.get());
            }
        else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Article not exists");
        }
        return articleToModify;
    }

    public Optional<Article> searchArticleByTitle(String title) {
        List<Article> publishedArticles = getAllPublishedArticles();
        for (Article searchedArt : publishedArticles) {
            int counter = 0;
            for (String wordOfGivenTitle : title.split(" ")) {
                String[] wordsOfSearchedArt = searchedArt.getTitle().split(" ");
                int condition = ((wordsOfSearchedArt.length / 2) + (wordsOfSearchedArt.length % 2));
                for (String word : wordsOfSearchedArt) {
                    // wordOfGivenTitle.equals(word)
                    if (this.distance.apply(wordOfGivenTitle.toLowerCase(), word.toLowerCase()) < 4) {
                        counter = counter + 1;
                        if (counter == condition) {
                            return Optional.of(searchedArt);
                        }
                    }
                }
            }
        }

        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Article not found, please be more specific");
    }


    public Optional<Article> deleteArticle(String id) {
        Optional<Article> article = this.articleRepository.findById(id);
        if (article.isPresent()) {
            this.articleRepository.deleteById(id);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The Id provided does not correspond to any existent article");
        }
        return article;
    }

}


