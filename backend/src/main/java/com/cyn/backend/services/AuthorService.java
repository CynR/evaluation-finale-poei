package com.cyn.backend.services;

import com.cyn.backend.controllers.representation.author.NewAuthorBody;
import com.cyn.backend.domain.Author;
import com.cyn.backend.repositories.AuthorRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Component
public class AuthorService {
    private AuthorRepository authorRepository;
    private IdGenerator idGenerator;

    public AuthorService(AuthorRepository authorRepository, IdGenerator idGenerator) {
        this.authorRepository = authorRepository;
        this.idGenerator = idGenerator;
    }

    public Optional<Author> getByMail(String mail) {
        Optional<Author> author = this.authorRepository.findByMail(mail);
        if (author.isPresent()) {
            return author;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        }

    }

    public List<Author> getAllAuthors() {
        return (List<Author>) this.authorRepository.findAll();
    }

    public Optional<Author> createAuthor(NewAuthorBody body) {
        if(body.getPseudo() != null || body.getMail() != null) {
            if (!doesAuthorExists(body.getMail())) {
                Author author = new Author(
                        this.idGenerator.generateNewId(),
                        body.getPseudo(),
                        body.getMail()
                );
                this.authorRepository.save(author);
                return Optional.of(author);
            }
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Unsuccessful creation. This mail already exists");
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Unsuccessful creation. Please insert valid and complete information");
    }

    public Boolean doesAuthorExists(String mail) {
        return this.authorRepository.findByMail(mail).isPresent();
    }

    public Optional<Author> deleteAuthor(String id) {
        Optional<Author> author = this.authorRepository.findById(id);
        if (author.isPresent()) {
            this.authorRepository.deleteById(id);
            return author;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The id provided does not correspond to any user");
        }

    }

}
