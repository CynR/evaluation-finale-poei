package com.cyn.backend.services;

import com.cyn.backend.domain.Tag;
import com.cyn.backend.repositories.TagRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Component
public class TagService {

    private TagRepository tagRepository;

    public TagService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    public List<Tag> getAllTags() {
        return (List<Tag>) this.tagRepository.findAll();
    }

    public Optional<Tag> getOneTagById(String id) {
        Optional<Tag> tag = this.tagRepository.findById(id);
        if (tag.isPresent()) {
            return tag;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Tag not found");
        }
    }

    public Optional<Tag> getTagByName(String tagName) {
        return this.tagRepository.findByTagName(tagName);
    }
    public Boolean doesTagExists(String tagName) {
        return this.tagRepository.findByTagName(tagName).isPresent();
    }

    public void saveTag(Tag tag){
        Tag newTag = new Tag(tag.getId(), tag.getTagName().toLowerCase());
        this.tagRepository.save(newTag);
    }

    public Optional<Tag> deleteTag(String id) {
        Optional<Tag> tag = this.tagRepository.findById(id);
        if (tag.isPresent()) {
            this.tagRepository.deleteById(id);
            return tag;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Unsuccessful delete, the id = %s does not correspond to any Tag", id));
        }
    }

}
