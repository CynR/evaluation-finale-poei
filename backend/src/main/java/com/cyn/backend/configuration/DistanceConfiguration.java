package com.cyn.backend.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.apache.commons.text.similarity.LevenshteinDistance;


@Configuration
public class DistanceConfiguration {

    @Bean
    public LevenshteinDistance distance() {
        return new LevenshteinDistance();
    }
}
